import { TestBed } from '@angular/core/testing';

import { RegularService } from './regular.service';

describe('RegularService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegularService = TestBed.get(RegularService);
    expect(service).toBeTruthy();
  });
});
