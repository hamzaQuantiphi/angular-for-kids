import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class RegularService {
  constructor() {}

  names: string[] = ["hamza","Moiyadi"];

  getNames(): Array<string> {
    return this.names;
  }

  setNames(names: string[]): void {
    
    this.names = names;
  }
}
