import { Component } from "@angular/core";
import { RegularService } from "./services/regular.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "angular-for-kids";

  data: object = {
    firstName: null,
    secondName: null,
    thirdName: null,
    fourthName: null,
    fifthName: null
  };

  constructor(private _regular: RegularService) {}

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }

  submitForm(): void {
    console.log("the form values are ", this.data);
    this._regular.setNames([
      this.data["firstName"],
      this.data["secondName"],
      this.data["thirdName"],
      this.data["fourthName"],
      this.data["fifthName"]
    ]);
  }
}
